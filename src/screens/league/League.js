import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { HeaderTab, LeagueItem, TeamItem } from '../../components'
import { Colors } from '../../consts'
import { getAllLeague } from '../../redux/actions'

const League = ({navigation}) => {
  const dispatch = useDispatch()
  const { allLeague } = useSelector(state => state.allLeagueReducer)

  useEffect(() => {
    dispatch(getAllLeague())
  }, [])

  const flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: Colors.SURFACE,
        }}
      />
    )
  }

  return (
    <>
    <SafeAreaView style={styles.topArea} />
    <View style={styles.container}>
      <HeaderTab title='League' />
      <FlatList
          data={allLeague.leagues}
          renderItem={({ item, index }) =>
            <LeagueItem 
              name={item.strLeague}
              onPress={() => navigation.navigate('Team', {league: item.strLeague})}
            />
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={flatListItemSeparator}
        />
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topArea: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  },
  input: {
    height: 40,
    margin: 12,
    width: '80%',
    borderWidth: 1,
    padding: 10,
  }
})

export default League
