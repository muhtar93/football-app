import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { Header, LeagueItem } from '../../components'
import { Colors } from '../../consts'
import { getLeague } from '../../redux/actions'

const LeagueByCountry = ({navigation, route}) => {
  const dispatch = useDispatch()
  const { league } = useSelector(state => state.leagueReducer)

  useEffect(() => {
    dispatch(getLeague(route.params.country))
  }, [])

  const flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          marginHorizontal: 8,
          backgroundColor: Colors.SURFACE,
        }}
      />
    )
  }

  return (
    <>
    <SafeAreaView style={styles.topArea} />
    <View style={styles.container}>
      <Header title='League' onPress={() => navigation.goBack()}/>
      <FlatList
          data={league.countrys}
          renderItem={({ item, index }) =>
            <LeagueItem 
              name={item.strLeague}
              onPress={() => navigation.navigate('Team', {league: item.strLeague})}
            />
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={flatListItemSeparator}
          showsVerticalScrollIndicator={false}
        />
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  topArea: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  },
  container: {
    flex: 1
  },
  input: {
    height: 40,
    margin: 12,
    width: '80%',
    borderWidth: 1,
    padding: 10,
  }
})

export default LeagueByCountry
