import React, { useEffect } from 'react'
import { Animated, Image, StatusBar, StyleSheet, View, ImageBackground, Text, Dimensions} from 'react-native'

const Splash = ({ navigation }) => {
  const fadeAnim = new Animated.Value(0)

  useEffect(() => {
    onSplash()
  })

  const onSplash = () => {
    Animated.timing(
      fadeAnim,
      {
        useNativeDriver: true,
        toValue: 1,
        duration: 3000
      }
    ).start(async () => {
      navigation.reset({
        index: 0,
        routes: [{ name: 'Bottom' }]
      })
    })
  }

  return (
    <Animated.View style={[styles.animated, { opacity: fadeAnim }]}>
      <ImageBackground source={require('../../images/splash_bg.png')} style={styles.image} resizeMode='cover'>
      <StatusBar backgroundColor='transparent' barStyle='default' translucent />
        <Image 
          source={require('../../images/ic_footballdb.png')}
          style={styles.icon}
        />
      </ImageBackground>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  animated: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 180,
    height: 30
  }
})

export default Splash