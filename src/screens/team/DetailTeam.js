import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView, Image, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { Gap, Header } from '../../components'
import { Colors, Fonts } from '../../consts'
import { getDetailTeam } from '../../redux/actions'

const DetailTeam = ({navigation, route}) => {
  const dispatch = useDispatch()
  const { detailTeam } = useSelector(state => state.detailTeamReducer)

  useEffect(() => {
    dispatch(getDetailTeam(route.params.id))
  }, [])


  const renderDetail = () => {
    if(detailTeam.teams !== undefined) {
      return (
        <View>
          <Image 
            source={{uri: detailTeam.teams[0].strStadiumThumb}}
            style={{height: 160, width: null}}
            resizeMode='center'
          />
          <View style={{width: 100, height: 100, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center', elevation: 5, alignSelf: 'center', marginTop: -40 }}>
          <Image 
            source={{uri: detailTeam.teams[0].strTeamBadge}}
            style={{height: 80, width: 80}}
            resizeMode='center'
          />
          <Text style={styles.teamName}>{detailTeam.teams[0].strTeam}</Text>
          </View>
          <Text style={styles.teamLeague}>{detailTeam.teams[0].strLeague}</Text>
          <Text style={styles.teamDesc}>{detailTeam.teams[0].strDescriptionEN}</Text>
          <Gap height={16} />
        </View>
      )
    }
  }

  return (
    <>
    <SafeAreaView style={styles.topArea} />
    <ScrollView showsVerticalScrollIndicator={false}>
    <View style={styles.container}>
      <Header title='Detail Team' onPress={() => navigation.goBack()} />
      {renderDetail()}
    </View>
    </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topArea: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  },
  teamName: {
    color: 'black',
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    marginTop: 16
  },
  teamLeague: {
    color: Colors.INACTIVE,
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    marginTop: 16,
    textAlign: 'center'
  },
  teamDesc: {
    color: Colors.SURFACE,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    marginTop: 16,
    marginHorizontal: 8
  },
})

export default DetailTeam
