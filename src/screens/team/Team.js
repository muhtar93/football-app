import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getTeam } from '../../redux/actions'
import { Colors } from '../../consts'
import {TeamItem, Header} from '../../components'

const Team = ({navigation, route}) => {
  const dispatch = useDispatch()
  const { team } = useSelector(state => state.teamReducer)

  useEffect(() => {
    dispatch(getTeam(route.params.league))
  }, [])

  const flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          backgroundColor: Colors.SURFACE,
          marginHorizontal: 8
        }}
      />
    )
  }

  return (
    <>
    <SafeAreaView style={styles.topArea} />
    <View style={styles.container}>
      <Header title='Team' onPress={() => navigation.goBack()}/>
      <FlatList
          data={team.teams}
          renderItem={({ item, index }) =>
            <TeamItem 
              name={item.strTeam}
              source={item.strTeamBadge}
              onPress={() => navigation.navigate('DetailTeam', {id: item.idTeam})}
            />
          }
          onEndReachedThreshold={0.4}
          ItemSeparatorComponent={flatListItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topArea: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  }
})

export default Team
