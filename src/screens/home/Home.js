import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView, Image, ImageBackground, StatusBar, TextInput } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { LeagueItem } from '../../components'
import { Colors, Types } from '../../consts'
import { getCountry } from '../../redux/actions'

const Home = ({navigation}) => {
  const dispatch = useDispatch()
  const { country } = useSelector(state => state.countryReducer)

  useEffect(() => {
    dispatch(getCountry())
  }, [])

  const flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          backgroundColor: Colors.SURFACE,
          marginHorizontal: 8
        }}
      />
    )
  }

  return (
    <>
    <SafeAreaView style={styles.safeAreaTop}/>
    <SafeAreaView style={[styles.container]}>
    <View style={styles.container}>
      <ImageBackground 
        source={require('../../images/banner_home.png')}
        resizeMode='cover'
        style={{height: 160, width: null}} 
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Image 
            source={require('../../images/ic_footballdb.png')}
            style={{width: null, height: 30, marginRight: '50%'}}
            resizeMode='contain'
          />  
        </View>
      </ImageBackground>
      <FlatList
          data={country.countries}
          renderItem={({ item, index }) =>
            <LeagueItem 
              name={item.name_en}
              onPress={() => navigation.navigate('LeagueByCountry', {country: item.name_en})}
            />
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={flatListItemSeparator}
        />
    </View>
    </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  safeAreaTop: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  },
  input: {
    height: 50,
    margin: 12,
    width: null,
    borderWidth: 1,
    padding: 10,
    borderRadius: 24,
    borderColor: '#CECECE',
    elevation: 10,
    shadowRadius: 4.65,
    shadowOffset: {
      height: 1,
      width: 1
    }
  }
})

export default Home
