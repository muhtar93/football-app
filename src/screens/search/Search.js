import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, SafeAreaView, TextInput } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getSearchlTeam } from '../../redux/actions'
import { Colors, Fonts } from '../../consts'
import {TeamItem, HeaderTab} from '../../components'

const Search = ({navigation}) => {
  const dispatch = useDispatch()
  const { searchTeam } = useSelector(state => state.searchTeamReducer)

  useEffect(() => {
    dispatch(getSearchlTeam(''))
  }, [])

  const flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          backgroundColor: Colors.SURFACE,
          marginHorizontal: 8
        }}
      />
    )
  }

  const onTypeSearch = (search) => {
    dispatch(getSearchlTeam(search))
  }

  return (
    <>
    <SafeAreaView style={styles.topArea} />
    <View style={styles.container}>
      <HeaderTab title='Search Team' onPress={() => navigation.goBack()}/>
      <TextInput 
        style={styles.input}
        placeholder='type your favorite team'
        onChangeText={(value) => onTypeSearch(value)}
      />
      <FlatList
          data={searchTeam.teams}
          renderItem={({ item, index }) =>
            <TeamItem 
              name={item.strTeam}
              source={item.strTeamBadge}
              onPress={() => navigation.navigate('DetailTeam', {id: item.idTeam})}
            />
          }
          onEndReachedThreshold={0.4}
          ItemSeparatorComponent={flatListItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topArea: {
    flex: 0,
    backgroundColor: Colors.PRIMARY
  },
  input: {
    height: 50,
    margin: 12,
    fontFamily: Fonts.REGULAR,
    width: null,
    borderWidth: 1,
    padding: 16,
    borderRadius: 24,
    borderColor: '#CECECE',
    elevation: 10,
    shadowRadius: 4.65,
    shadowOffset: {
      height: 1,
      width: 1
    }
  }
})

export default Search
