import { combineReducers } from 'redux'
import { countryReducer } from './Country'
import { leagueReducer, allLeagueReducer } from './League'
import { peopleReducer } from './People'
import { detailReducer } from './Detail'
import { globalReducer } from './Global'
import { teamReducer, detailTeamReducer, searchTeamReducer } from './Team'

const reducer = combineReducers({
  peopleReducer,
  detailReducer,
  globalReducer,
  countryReducer,
  leagueReducer,
  teamReducer,
  detailTeamReducer,
  searchTeamReducer,
  allLeagueReducer
})

export default reducer