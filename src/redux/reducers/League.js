import { Types } from '../../consts'

const initLeague = {
  league: []
}

export const leagueReducer = (state = initLeague, action) => {
  if (action.type === Types.LEAGUE) {
    return {
      ...state,
      league: action.value
    }
  }

  return state
}

const initAllLeague = {
  allLeague: []
}

export const allLeagueReducer = (state = initAllLeague, action) => {
  if (action.type === Types.ALL_LEAGUE) {
    return {
      ...state,
      allLeague: action.value
    }
  }

  return state
}