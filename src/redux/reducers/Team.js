import { Types } from '../../consts'

const initTeam = {
  team: []
}

export const teamReducer = (state = initTeam, action) => {
  if (action.type === Types.TEAM) {
    return {
      ...state,
      team: action.value
    }
  }

  return state
}

const initDetailTeam = {
  detailTeam: []
}

export const detailTeamReducer = (state = initDetailTeam, action) => {
  if (action.type === Types.DETAIL_TEAM) {
    return {
      ...state,
      detailTeam: action.value
    }
  }

  return state
}

const initSearchTeam = {
  searchTeam: []
}

export const searchTeamReducer = (state = initSearchTeam, action) => {
  if (action.type === Types.SEARCH_TEAM) {
    return {
      ...state,
      searchTeam: action.value
    }
  }

  return state
}