import { Types } from '../../consts'

const initCountry = {
  country: []
}

export const countryReducer = (state = initCountry, action) => {
  if (action.type === Types.COUNTRY) {
    return {
      ...state,
      country: action.value
    }
  }

  return state
}