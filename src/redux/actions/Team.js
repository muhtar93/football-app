import { Networks, Types } from '../../consts'
import axios from 'axios'
import { setLoading } from './Global'

export const getTeam = (league) => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/search_all_teams.php?l=${league}`)
    .then(res => {
      console.log('getTeam', res.data)
      dispatch({ type: Types.TEAM, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}

export const getDetailTeam = (id) => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/lookupteam.php?id=${id}`)
    .then(res => {
      console.log('detailTeam', res.data)
      dispatch({ type: Types.DETAIL_TEAM, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}

export const getSearchlTeam = (team) => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/searchteams.php?t=${team}`)
    .then(res => {
      console.log('getSearchlTeam', res.data)
      dispatch({ type: Types.SEARCH_TEAM, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}