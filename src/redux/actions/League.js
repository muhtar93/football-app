import { Networks, Types } from '../../consts'
import axios from 'axios'
import { setLoading } from './Global'

export const getAllLeague = () => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/all_leagues.php`)
    .then(res => {
      console.log('getLeague', res.data)
      dispatch({ type: Types.ALL_LEAGUE, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error getLeague', err)
      dispatch(setLoading(false))
    })
}

export const getLeague = (country) => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/search_all_leagues.php?c=${country}`)
    .then(res => {
      console.log('getLeague', res.data)
      dispatch({ type: Types.LEAGUE, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}