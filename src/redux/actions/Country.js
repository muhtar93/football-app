import { Networks, Types } from '../../consts'
import axios from 'axios'
import { setLoading } from './Global'

export const getCountry = () => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}/all_countries.php`)
    .then(res => {
      console.log('res', res.data)
      dispatch({ type: Types.COUNTRY, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}