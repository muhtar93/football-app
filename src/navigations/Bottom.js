import 'react-native-gesture-handler';
import * as React from 'react'
import {
  createStackNavigator
} from '@react-navigation/stack';
import {
  createBottomTabNavigator
} from '@react-navigation/bottom-tabs';
import Home from '../screens/home/Home'
import Team from '../screens/team/Team'
import { Image, View } from 'react-native';
import { Colors } from '../consts';
import League from '../screens/league/League';
import Search from '../screens/search/Search';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const IconBottom = (props) => {
  const { color, focused } = props.data
  let colorSelected = focused ? color : Colors.INACTIVE

  return (
      <View>
            <Image source={props.image} style={{ width: 30, height: 30, tintColor: colorSelected }} />
      </View>
  )
}

const Bottom =  () => {
  return (
      <Tab.Navigator
        initialRouteName='Home'
        tabBarOptions={{
          activeTintColor: Colors.PRIMARY,
          showLabel: false
        }}>
        <Tab.Screen
          name='Home'
          component={Home}
          options={{
            tabBarIcon: (props) => (
              <IconBottom data={props} image={require('../images/ic_home.png')} />
            )
          }}  />
        <Tab.Screen
          name='League'
          component={League}
          options={{
            tabBarIcon: (props) => (
              <IconBottom data={props} image={require('../images/ic_league.png')} />
            )
          }} />
        <Tab.Screen
          name='Search'
          component={Search}
          options={{
            tabBarIcon: (props) => (
              <IconBottom data={props} image={require('../images/ic_search.png')} />
            )
        }} />
      </Tab.Navigator>
  );
}

export default Bottom