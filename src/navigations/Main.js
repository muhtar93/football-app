import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import People from '../screens/people/People'
import Detail from '../screens/detail/Detail'
import Splash from '../screens/splash/Splash'
import Home from '../screens/home/Home'
import Team from '../screens/team/Team'
import DetailTeam from '../screens/team/DetailTeam'
import League from '../screens/league/League'
import LeagueByCountry from '../screens/league/LeagueByCountry'
import Bottom from '../navigations/Bottom'

const Stack = createStackNavigator()

const Main = () => {
  const options = {
    gestureEnabled: true,
    ...TransitionPresets.SlideFromRightIOS
  }

  return (
    <Stack.Navigator
      initialRouteName='Splash'
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}>
      <Stack.Screen name='Splash' component={Splash} options={options} />
      <Stack.Screen name='Bottom' component={Bottom} options={options} />
      <Stack.Screen name='People' component={People} options={options} />
      <Stack.Screen name='Detail' component={Detail} options={options} />
      <Stack.Screen name='Home' component={Home} options={options} />
      <Stack.Screen name='Team' component={Team} options={options} />
      <Stack.Screen name='DetailTeam' component={DetailTeam} options={options} />
      <Stack.Screen name='League' component={League} options={options} />
      <Stack.Screen name='LeagueByCountry' component={LeagueByCountry} options={options} />
    </Stack.Navigator>
  )
}

export default Main