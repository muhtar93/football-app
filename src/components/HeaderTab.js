import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Colors, Fonts } from '../consts'

const HeaderTab = ({ title, onPress }) => {
  return (
    <View style={styles.container}>
        <Text style={styles.text}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    shadowColor: Colors.BROWN_GREY_TWO,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 3,
    backgroundColor: Colors.PRIMARY,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: 'white',
    fontFamily: Fonts.BOLD,
    fontSize: 20,
    justifyContent: 'center'
  }
})

export default HeaderTab