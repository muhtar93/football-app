import Gap from './Gap'
import Loading from './Loading'
import PeopleItem from './PeopleItem'
import SubItem from './SubItem'
import Header from './Header'
import DetailItem from './DetailItem'
import Button from './Button'
import ListItem from './ListItem'
import LeagueItem from './LeagueItem'
import TeamItem from './TeamItem'
import HeaderTab from './HeaderTab'

export {
  Gap,
  Loading,
  PeopleItem,
  SubItem,
  Header,
  DetailItem,
  Button,
  ListItem,
  LeagueItem,
  TeamItem,
  HeaderTab
}