const Colors = {
  BLACK: '#000000',
  WHITE: 'rgba(0, 0, 0, 0.5)',
  VERY_LIGHT_PINK_TWO: 'rgb(233,233,233)',
  PRIMARY: '#3a69af',
  INACTIVE: '#aaadb5',
  SURFACE: '#55555d'
}

export default Colors
